<h1>Compra Rápida APP</h1>

Para rodar o projeto é necessario possuir um emulador android

**Aplicativo para usuário**

**Primeira etapa** do aplicativo do usuário é o acesso com login e senha.

Exemplo:

Login: **teste@email.com**

Senha: **teste123**

Também é possível realizar o acesso atráves das redes sociais, para aqueles que possuem uma
conta no Facebook ou Google poderá acessar o app normalmente sem necessitar criar um novo login.

Após realizar o acesso com login e senha corretamente, abrirá a tela principal (home) 
nela é possível abrir a sessão de Menu onde terá acesso à alguns dados como:

- Mensagens
- Pagamentos
- Listas
- Contato

Na **segunda etapa** ainda na tela principal teremos então as ultímas listas que foram geradas 
e também os ultímos pedidos realizados.

Em listas temos:

- Produtos
- Marca
- Quantidade

Em pedidos temos:

- Quantidade de itens
- Nome do supermercado
- Data da realização da compra
- Status do pedido
- Nome do entregador
- Chat
- Tempo do pedido

Na nossa **terceira etapa** passamos a criar uma nova lista, clicando no icone de **+** abrirá uma nova tela 
com campo de pesquisa onde poderá procurar por itens e adicionalo ao carrinho.
Após adicionar um item ele abrirará um icone ao lado com novamente um sinal de + onde será inserido a 
a marca do produto ou tipo do produto por exemplo:

_**Bananas verdes**_

Depois de escolher o produto e a marca/tipo será inserido a quantidade desses produtos que desejas.
Ao escolher todos itens e estar com a lista pronta basta selecionar o icone de seta direita (**->**) 

**Quarta etapa** é preciso escolher o supermercado a qual deseja que sejam feitas as compras, também possível editar
novamente a lista caso desista ou esqueça de algum item.

Depois de escolher o local o app pede o endereço que será entregue os produtos, ao apertar na caixa de mensagem 
abrirá novamente outra tela onde é possível digitar o endereço ou até mesmo escolher no mapa o local exato da entrega,
depois basta confirmar o endereço.

Na **quinta etapa** e ultima é onde mostrará todas informações do pedido como:

- Quantidade
- Supermercado
- Endereço escolhido
- Data do pedido
- Situação do pedido
- Nome do entregador

Ao encontrar o entregador basta confirmar e aguardar chegar em casa. Em caso de dúvida também tem o icone de mensagem 
na qual é só apertar para abrir um chat entre você e o responsável pela suas compras e entrega.

[APK para download](https://gitlab.com/projeto-de-desenvolvimento/compra-rapido/app/-/blob/f9d116141d7af42a57a76d7ad9d47f9a028196ab/app-release.apk)

